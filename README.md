This program was written during my master thesis. It is used to generate STL files of robotic grippers. The program is written in Matlab using the SG library by Prof. Lüth.


Example of linear gripper:
![](https://i.imgur.com/2oxgpbs.png)

Example of angled gripper:
![](https://i.imgur.com/7yXAUY2.png)

Example of compliant gripper:
![](https://i.imgur.com/6GfruwF.png)


[Example video of compliant gripper:](https://i.imgur.com/2isR2Uj.mp4)

[Example of gripper GUI](https://i.imgur.com/BxI7DVQ.mp4)
